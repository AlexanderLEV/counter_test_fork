import Counter from './counter';

const defaultConfig = {
  name: 'Cool Counter',
  inc: 1,
  initValue: 1
}


let elem = null;
function createElem() {
  elem = document.createElement('div');
  elem.id = 'root';
  document.body.appendChild(elem);
}

function destroyElem() {
  document.body.removeChild(elem);
  elem = null;
}

let counterCmp;
function createComponent() {
  counterCmp = new Counter(elem, defaultConfig);
}

function destroyComponent() {
  counterCmp = null;
}

//Counter
describe('Counter', () => {
  beforeEach(createElem);
  afterEach(destroyElem);
  // beforeEach beforeAll afterEach afterAll
  it('should ne created', () => {

    const counter = new Counter(
      elem,
      defaultConfig
    );

    expect(counter).toBeDefined();
    expect(counter.name).toMatch(defaultConfig.name);
    expect(counter.inc).toEqual(defaultConfig.inc);
    expect(counter.value).toEqual(defaultConfig.initValue);
  })

  it('should ne created with default options', () => {
    const counter = new Counter(elem);

    expect(counter).toBeDefined();
    expect(counter.name).toMatch('');
    expect(counter.inc).toEqual(1);
    expect(counter.value).toEqual(0);
  })

  it('should throw', () => {
    expect(function () {
      new Counter();
    }).toThrow();
  })
});


//Render
describe('test initial methods', () => {

  beforeEach(() => {
    createElem();
    createComponent();
  });
  afterEach(() => {
    destroyElem();
    destroyComponent();
  });

  describe('test Counter render', () => {
    it('should create root element', () => {
      expect(counterCmp.cmpRoot instanceof HTMLElement).toBe(true);
    })

    it('should have correct title', () => {
      const title = counterCmp.cmpRoot.getElementsByClassName('counter__title')[0].innerText;
      expect(title).toMatch(defaultConfig.name);
    })

    it('should have correct value', () => {
      const value = counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText;
      expect(value).toMatch(defaultConfig.initValue + '');
    })
  })
  
  describe('Test initEvents', () => {
    beforeEach(() => {
      destroyComponent();
    })
    
    it('should call initEvents', () => {
      const eventsSpy = spyOn(Counter.prototype, 'initEvents');
      createComponent();
      expect(eventsSpy).toHaveBeenCalled();
    })
    
    it('should increment', () => {
      const incrementSpy = spyOn(Counter.prototype, 'onIncrement').and.callThrought();
      createComponent();
      
      const e = new MouseEvent('click');
      counterCmp.cmpRoot.getElementsByClassName('js-increment')[0].dispatchEvent(e);
      
      expect(incrementSpy).toHaveBeenCalled();
    })
    
    it('should value incremented', () => {
      const incrementSpy = spyOn(Counter.prototype, 'onIncrement').and.callThrough();
      createComponent();
      const oldValue = counterCmp.value;
      
      const e = new MouseEvent('click');
      counterCmp.cmpRoot.getElementsByClassName('js-increment')[0].dispatchEvent(e);
      
      expect(counterCmp.value).toEqual(oldValue + 1);
    })
  })
  
  fdescribe('Set max', () => {
    it('should set max value on click', () => {
      destroyComponent();
      const setMaxSpy = spyOn(Counter.prototype, 'onSetMax').and.callThrough();
      createComponent();
      
      const e = new MouseEvent('click');
      counterCmp.cmpRoot.getElementsByClassName('js-set-max')[0].dispatchEvent(e);
      
      expect(setMaxSpy).toHaveBeenCalled();
      expect(counterCmp.value).toEqual(counterCmp.maxVal);
      expect(+counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText).toEqual(counterCmp.maxVal);
    })
    
    it('should set min value on click', () => {
      destroyComponent();
      const setMinSpy = spyOn(Counter.prototype, 'onSetMin').and.callThrough();
      createComponent();
      
      const e = new MouseEvent('click');
      counterCmp.cmpRoot.getElementsByClassName('js-set-min')[0].dispatchEvent(e);
      
      expect(setMinSpy).toHaveBeenCalled();
      expect(counterCmp.value).toEqual(1);
      expect(+counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText).toEqual(defaultConfig.initValue);
    })
  })
  
})